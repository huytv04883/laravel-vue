import { db } from "../../api/firebaseDb";


const state = () => ({
    detail: {
        listDetail  : [],
        cart       : [],
    },
})

const actions = {
    getDataProductDetail ({ commit }) {
        db.collection('productDetail').onSnapshot((itemDetai) => {
            let listDetail = [];
            itemDetai.forEach((item) => {
                listDetail.push({
                    key     : item.id,
                    image   : item.data().image,
                    color   : item.data().color,
                    detail  : item.data().details,
                    size    : item.data().size,
                    quantity: item.data().quantity,
                    sku     : item.data().sku,
                    name    : item.data().name,
                    price     : item.data().price
                })
            });
            commit("setProductDetail", listDetail);
        });
    },
    addProductTocart ({commit, state} , {product,val}) {
        if (product) {
            const cartItem = state.detail.cart.find(item => item.key == product.key);
            if (!cartItem) {
                commit('pushItemToCart', {key: product.key, item: product});
            } else {
                commit('imcrementProductQuantity' , {cartItem ,val:val})
            }
        }
    }
}

const mutations = {
    setProductDetail(state, val) {
        state.detail.listDetail = val;
    },
    // add-to-cart
    pushItemToCart (state , {key, item}) {
        state.detail.cart.push({
            item,
            key: key,
            quantity: 1,
        });
    },
    imcrementProductQuantity (state, {cartItem, val}) {
        cartItem.quantity+=val;
    }
}

const getters = {
    getDetail: (state) => (key) => {
        return state.detail.listDetail.filter(item => item.key === key);
    },
    cartProduct: (state)  => {
        return state.detail.cart
    }, 
    totalPriceProduct: (state, getters) => {
        return getters.cartProduct.reduce((currentTotal, itemProduct) => {
            return currentTotal + itemProduct.item.price * itemProduct.quantity
        },0)
    },
    totalNumberProduct: (state, getters) => {
        return getters.cartProduct.reduce((currentTotal, itemProduct) => {
            return currentTotal + itemProduct.quantity
        },0)
    },
}

export default {
    state,
    actions,
    mutations,
    getters
}

