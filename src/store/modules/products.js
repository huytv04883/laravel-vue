import { db } from "../../api/firebaseDb";


const state = () => ({
    product: {
        listProducts: [],
    },
})

const actions = {
    getdata ({ commit }) {
        db.collection('products').onSnapshot((itemProduct) => {
            let list = [];
            itemProduct.forEach((item) => {
                list.push({
                    key   : item.id,
                    image : item.data().images,
                    name  : item.data().name,
                    price : item.data().price,
                })
            });
            commit("setProduct", list);
        });
    },
}

const mutations = {
    setProduct(state, val) {
        state.product.listProducts = val;
    },
}

const getters = {
    listData: (state) => {
        return state.product.listProducts
    },
    getProductById: (state) => (key) => {
        return state.product.listProducts.filter(item => item.key === key);
    },
}

export default {
    state,
    actions,
    mutations,
    getters
}


