import { db } from "../../api/firebaseDb";


const state = () => ({
    detail: {
        listDetail  : [],
        cart       : [],
    },
    validate: {
        messages: {
            errorSize: '',
            errorColor: '',
        }
    }
})

const actions = {
    getDataProductDetail ({ commit }) {
        db.collection('productDetail').onSnapshot((itemDetai) => {
            let listDetail = [];
            itemDetai.forEach((item) => {
                listDetail.push({
                    key     : item.id,
                    image   : item.data().image,
                    color   : item.data().color,
                    detail  : item.data().details,
                    size    : item.data().size,
                    quantity: item.data().quantity,
                    sku     : item.data().sku,
                    name    : item.data().name,
                    price     : item.data().price
                })
            });
            commit("setProductDetail", listDetail);
        });
    },
    addProductTocart ({commit, state} , {product,val,sizeSelected,colorSelected}) {
        if (product) {
            const cartItem = state.detail.cart.find(item => item.key == product.key);
            if (!cartItem) {
                commit('pushItemToCart', {key: product.key, item: product , size: sizeSelected , color:colorSelected});
            } else {
                commit('imcrementProductQuantity' , {cartItem ,val:val})
            }
        }
    },
    removeItemCart ({commit} , {index}) {
        commit('decrementItemCart', {index: index});
    }
}

const mutations = {
    setProductDetail(state, val) {
        state.detail.listDetail = val;
    },
    // add-to-cart
    pushItemToCart (state , {key, item, size , color}) {
        if (color == null) {
            state.validate.errorColor = 'Please select color !';
        } else if (size == null) {
            state.validate.errorSize = 'Please select size !';
            state.validate.errorColor= '';
        } else {
            state.detail.cart.push({
                item,
                key: key,
                quantity: 1,
                size,
                color,
            });
            state.validate.errorColor= '';
            state.validate.errorSize = '';
        }
    },
    imcrementProductQuantity (state, {cartItem, val}) {
        cartItem.quantity+=val;
    },
    decrementItemCart (state , {index}) {
        state.detail.cart.splice(index,1);
    }
}

const getters = {
    getError: (state) => {
        return state.validate;
    },
    getDetail: (state) => (key) => {
        return state.detail.listDetail.filter(item => item.key === key);
    },
    cartProduct: (state)  => {
        return state.detail.cart
    }, 
    totalPriceProduct: (state, getters) => {
        return getters.cartProduct.reduce((currentTotal, itemProduct) => {
            return currentTotal + itemProduct.item.price * itemProduct.quantity
        },0)
    },
    totalNumberProduct: (state, getters) => {
        return getters.cartProduct.reduce((currentTotal, itemProduct) => {
            return currentTotal + itemProduct.quantity
        },0)
    },
}

export default {
    state,
    actions,
    mutations,
    getters
}

