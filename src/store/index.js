import { createStore } from 'vuex'
import products from './modules/products'
import productDetail from './modules/productDetail'
const store = createStore({
    modules: {
        products,
        productDetail
    }
})
store.dispatch('getdata');
store.dispatch('getDataProductDetail');
export default store;