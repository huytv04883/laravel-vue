import firebase from  'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyAiqoc1-rnN241siIKhtihytfqtNiCNDCI",
    authDomain: "project1-b83f1.firebaseapp.com",
    projectId: "project1-b83f1",
    storageBucket: "project1-b83f1.appspot.com",
    messagingSenderId: "182234898479",
    appId: "1:182234898479:web:d167fdf690a8fa5106a6da",
    measurementId: "G-WWPR0217HY"
}

const firebaseApp = firebase.initializeApp(firebaseConfig);

export const db = firebaseApp.firestore();